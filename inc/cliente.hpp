#ifndef CLIENTE_HPP 
#define CLIENTE_HPP
#include <string>

using namespace std;

class Cliente{
private:
    string cpf;
    string nome;
    bool premium;
public:
    Cliente();
    Cliente(string cpf, string nome, bool premium);
    ~Cliente();
    void set_cpf(string cpf);
    string get_cpf();
    void set_nome(string nome);
    string get_nome();
    void set_premium(bool premium);
    bool get_premium();

 
};
#endif